# Title: Single Price Grid Component

Tech stack: React & Vite

Deployed project: https://saad-shaikh-single-price-grid-component.netlify.app/

## Desktop view:
![Desktop view](design/desktop-design.jpg) 

## Mobile view:
![Mobile view](design/mobile-design.jpg) 