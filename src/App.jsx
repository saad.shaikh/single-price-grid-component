import { Fragment } from 'react'
import './App.css'

let App = () => {

  return (
    <Fragment>
      <main className='card'>

        <section className='section-1'>
          <h2>Join our community</h2>
          <h3>30-day, hassle-free money back guarantee</h3>
          <p>
            Gain access to our full library of tutorials along with expert code reviews.
            Perfect for any developers who are serious about honing their skills.
          </p>
        </section>

        <section className='section-2'>
          <h2>Monthly Subscription</h2>
          <div className='price'>
            <span className='amount'>&#36;29</span>&#160;
            <span className='period'>per month</span>
          </div>
          <p>Full access for less than &#36;1 a day</p>
          <button>Sign Up</button>
        </section>

        <section className='section-3'>
          <h2>Why Us</h2>
          <ul>
            <li>Tutorials by industry experts</li>
            <li>Peer &amp; expert code review</li>
            <li>Coding exercises</li>
            <li>Access to our GitHub repos</li>
            <li>Community forum</li>
            <li>Flashcard decks</li>
            <li>New videos every week</li>
          </ul>
        </section>

      </main>

      <footer>
        <p class="attribution">
          Challenge by <a href="https://www.frontendmentor.io/solutions/single-price-grid-component-react-and-vite-1DCeYz5zTN" target="_blank">Frontend Mentor</a>.
          Coded by <a href="https://saad-shaikh-portfolio.netlify.app/" target="_blank">Saad Shaikh</a>.
        </p>
      </footer>
    </Fragment>
  )
}

export default App
